# Autonomous Swarm

Collaborative visual area coverage by autonomous mobile aerial agents, equipped with PTZ cameras and operating under localization uncertainty.